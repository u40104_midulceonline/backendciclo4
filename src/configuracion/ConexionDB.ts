import {connect} from "mongoose";

const ConexionDB = ()=> {
    const urlConexion=String(process.env.db_mongo);
    connect(urlConexion)
    .then(()=>{
        console.log("Conectados a la base de datos",process.env.db_mongo);
    })
    .catch((miError)=>{
        console.log("NO se puede establecer una conexion",miError);
    });

};
export default ConexionDB;