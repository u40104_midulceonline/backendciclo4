import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";
import express from "express";
import ConexionDB from "./ConexionDB";

//Aca van los import de lass rutas
import perfilRuta from "../ruta/PerfilRuta"
import usuarioRuta from "../ruta/UsuarioRuta"

class Servidor {
  public app:express.Application

  constructor() {
    dotenv.config({path: "variables.env" }) //´carga las variables para poder usarlas
    ConexionDB();
    this.app = express();
    this.iniciarConfiguracion();
    this.iniciarRutas();
  }

  public iniciarConfiguracion(){
    this.app.set("PORT",process.env.PORT)
    this.app.use(cors()) //bloquear o desbloquear que usen el back
    this.app.use(morgan("dev")) // mantenga un bloque de peticiones
    this.app.use(express.json({limit:"100Mb"})) //limitar carag de archivos
    this.app.use(express.urlencoded({extended:true})) 
  }

  public iniciarRutas(){
    this.app.use("/api/perfiles",perfilRuta)
    this.app.use("/api/usuario",usuarioRuta)
  }

  public iniciarServidor(){
    this.app.listen(this.app.get("PORT"),()=>{
      console.log("Servidor Backend",this.app.get("PORT"))
    })


  }
}

export default Servidor;
