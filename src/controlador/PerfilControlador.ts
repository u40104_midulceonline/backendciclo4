import PerfilDao from "../dao/PerfilDao";
import { Request, Response } from "express";

class PerfilControlador extends PerfilDao {
  public consulta(req: Request, res: Response): void {
    //llamamos al dao
    PerfilControlador.obtenerPerfiles(res);
  }

  public crear(req: Request, res: Response): void {
    //llamamos al dao
    PerfilControlador.crearPerfil(req.body, res);
  }

  public eliminar(req: Request, res: Response): void {
    //llamamos al dao
    PerfilControlador.eliminarPerfil(req.params.codigo, res);
  }

  public actualizar(req: Request, res: Response): void {
    //llamamos al dao
    PerfilControlador.actualizarPerfil(req.params.codigo, req.body, res);
  }

}

const perfilControlador = new PerfilControlador(); //crear objeto tipo controlador
export default perfilControlador;
