import UsuarioDao from "../dao/UsuarioDao";
import { Request, Response } from "express";

class UsuarioControlador extends UsuarioDao {
  public consulta(req: Request, res: Response): void {
    //llamamos al dao
    UsuarioControlador.obtenerUsuarios(res);
  }

  public crear(req: Request, res: Response): void {
    const elCorreo={correoUsuario:req.body.correoUsuario}
    //llamamos al dao
    UsuarioControlador.crearUsuario(elCorreo,req.body, res);
  }

  public eliminar(req: Request, res: Response): void {
    //llamamos al dao
    UsuarioControlador.eliminarUsuario(req.params.codigo, res);
  }

  public actualizar(req: Request, res: Response): void {
    //llamamos al dao
    UsuarioControlador.actualizarUsuario(req.params.codigo, req.body, res);
  }

}

const usuarioControlador = new UsuarioControlador(); //crear objeto tipo controlador
export default usuarioControlador;
