import {config} from "dotenv"
import { Router } from "express";
import usuarioControlador from "../controlador/UsuarioControlador";

class UsuarioRuta {
  public rutaApiUsuario: Router

  constructor() {
    this.rutaApiUsuario = Router()
    this.configuracion();
  }
  // enpoints

  public configuracion(): void {
    this.rutaApiUsuario.get("/todos", usuarioControlador.consulta)
    this.rutaApiUsuario.post("/nuevo", usuarioControlador.crear)
    this.rutaApiUsuario.delete("/eliminar/:codigo", usuarioControlador.eliminar)
    this.rutaApiUsuario.put("/actualizar/:codigo", usuarioControlador.actualizar)
  }
  
}

const usuarioRuta = new UsuarioRuta();
export default usuarioRuta.rutaApiUsuario;
