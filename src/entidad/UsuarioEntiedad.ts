import { PerfilEntidad } from "./PerfilEntidad";

export class UsuarioEntidad {
  public nombreUsuario: string;
  public correoUsuario: string;
  public claveUsuario: string;
  public fechaRegistroUsuario: Date;
  public estadoUsuario: number;
  public codPerfil: PerfilEntidad;

  constructor(
    nom: string,
    cor: string,
    cla: string,
    fec: Date,
    est: number,
    cod: PerfilEntidad
  ) {
    this.nombreUsuario = nom;
    this.correoUsuario = cor;
    this.claveUsuario = cla;
    this.fechaRegistroUsuario = fec;
    this.estadoUsuario = est;
    this.codPerfil = cod;
  }
}
export default UsuarioEntidad;
