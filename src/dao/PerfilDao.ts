// se conecta a la base de datos para hacer CRUD (Create,read, update,delete)
import { response, Response } from "express";
import PerfilEsquema from "../esquema/PerfilEsquema";
import {Types, ObjectId} from "mongoose"

class PerfilDao {
  // Enpoint Obtener perfil
  protected static async obtenerPerfiles(res: Response): Promise<any> {
    const datos = await PerfilEsquema.find().sort({ _id: -1 });
    res.status(200).json(datos);
  }

  //Enpoint crear
  protected static async crearPerfil(
    parametros: any,
    res: Response
  ): Promise<any> {
    //validar la existencia del dato
    const existe = await PerfilEsquema.findOne(parametros);
    if (existe) {
      res.status(400).json({ respuesta: "El perfil ya existe" });
    } else {
      const objPerfil = new PerfilEsquema(parametros); // objeto perfil esquema
      objPerfil.save((miError, miObjeto) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede crear el perfil" });
        } else {
          res
            .status(200)
            .json({ respuesta: "Perfil creado", codigo: miObjeto._id });
        }
      });
    }
  }

  //Eliminar
  protected static async eliminarPerfil(
    parametro: any,
    res: Response
  ): Promise<any> {
    const existe = await PerfilEsquema.findById(parametro).exec();
    if (existe) {
      PerfilEsquema.findByIdAndDelete( parametro , (miError: any, miObjeto: any) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede eliminar el perfil" });
        } else {
          res
            .status(200)
            .json({
              resultado: "Perfil eliminado:",
              eliminado: miObjeto
            });
        }
      });
    } else {
      res.status(400).json({ respuesta: "El perfil no existe" });
    }
  }

  protected static async actualizarPerfil(
    codigo: any,
    parametros: any,
    res: Response
  ): Promise<any> {
      //const nuevoCodigo = new Types.ObjectId (codigo)
    //const existe = await PerfilEsquema.findById(codigo);
    //const existe = await PerfilEsquema.findById({_id:codigo});
    const existe = await PerfilEsquema.findById(codigo).exec();
    if (existe) {
      PerfilEsquema.findByIdAndUpdate(
        {_id:codigo}, 
        {$set:parametros},
        (miError: any, miObjeto: any) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede actualizar el perfil" });
        } else {
          res
            .status(200)
            .json({
              resultado: "Perfil Actualizado:",
              antiguo: miObjeto,
              nuevo: parametros
            });
        }
      });
    } else {
      res.status(400).json({ respuesta: "El a actualizar No existe" });
    }
  }

}

export default PerfilDao;
