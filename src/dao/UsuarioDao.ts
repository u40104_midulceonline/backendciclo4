// Crear un usuario
//import {Types, ObjectId} from "mongoose"

import { Response } from "express";
import UsuarioEsquema from "../esquema/UsuarioEsquema";
import cifrar from "bcryptjs"
import jwt from "jsonwebtoken";

class UsuarioDao {
  // Enpoint Obtener perfil
  protected static async obtenerUsuarios(res: Response): Promise<any> {
    const datos = await UsuarioEsquema.find().sort({ _id: -1 });
    res.status(200).json(datos);
  }

  //Enpoint crear
  protected static async crearUsuario(
    correo:any,
    parametros: any,
    res: Response
  ): Promise<any> {
    //validar la existencia del dato
    const existe = await UsuarioEsquema.findOne(correo);
    if (existe) {
      res.status(400).json({ respuesta: "El correo ya existe" });
    } else {
      parametros.claveUsuario=cifrar.hashSync(parametros.claveUsuario,8)
      const objUsuario = new UsuarioEsquema(parametros); // objeto perfil esquema
      objUsuario.save((miError, miObjeto) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede crear el usuario" });
        } else {
          const datosVisibles={
            codUsuario:miObjeto._id,
            correo:correo
          }
          const llavePrivada=String(process.env.clave_secreta)
          const miToken= jwt.sign(datosVisibles,llavePrivada,{expiresIn:86400}) //habilitar tocken
          res
            .status(200)
            .json({ token:miToken });
        }
      });
    }
  }

  //Eliminar
  protected static async eliminarUsuario(
    parametro: any,
    res: Response
  ): Promise<any> {
    const existe = await UsuarioEsquema.findById(parametro).exec();
    if (existe) {
      //UsuarioEsquema.findByIdAndDelete( parametro , (miError: any, miObjeto: any)
      UsuarioEsquema.findByIdAndDelete( parametro , (miError: any, miObjeto: any) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede eliminar el usuario" });
        } else {
          res
            .status(200)
            .json({
              resultado: "Usuario eliminado:",
              eliminado: miObjeto
            });
        }
      });
    } else {
      res.status(400).json({ respuesta: "El usuario no existe" });
    }
  }

  protected static async actualizarUsuario(
    codigo: any,
    parametros: any,
    res: Response
  ): Promise<any> {
      //const nuevoCodigo = new Types.ObjectId (codigo)
    //const existe = await PerfilEsquema.findById(codigo);
    //const existe = await PerfilEsquema.findById({_id:codigo});
    const existe = await UsuarioEsquema.findById(codigo).exec();
    if (existe) {
      UsuarioEsquema.findByIdAndUpdate(
        {_id:codigo}, 
        {$set:parametros},
        (miError: any, miObjeto: any) => {
        if (miError) {
          res.status(400).json({ respuesta: "No se puede actualizar el usuario" });
        } else {
          res
            .status(200)
            .json({
              resultado: "Usuario Actualizado:",
              antiguo: miObjeto,
              nuevo: parametros
            });
        }
      });
    } else {
      res.status(400).json({ respuesta: "El usuario a actualizar NO existe" });
    }
  }

}

export default UsuarioDao;
