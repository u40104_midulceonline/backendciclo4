import { model, Schema, Types } from "mongoose";
import { UsuarioEntidad } from "./../entidad/UsuarioEntiedad";

const UsuarioEsquema = new Schema<UsuarioEntidad>(
  {
    nombreUsuario: { type: String, required: true, trim: true },
    correoUsuario: {
      type: String,
      unique: true,
      required: true,
      lowercase: true,
    },
    claveUsuario: { type: String, required: true },
    fechaRegistroUsuario: { type: Date, default: Date.now() },
    estadoUsuario: { type: Number, enum: [1, 2], default: 1 },
    codPerfil: { type: Types.ObjectId, ref: "Perfil", required: true }, // relacion "Perfil" --> nombre_Coleccion MongoDB
  },
  { versionKey: false }
);

export default model("Usuario", UsuarioEsquema, "Usuario");
