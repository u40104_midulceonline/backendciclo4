"use strict";
// Crear un usuario
//import {Types, ObjectId} from "mongoose"
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioEsquema_1 = __importDefault(require("../esquema/UsuarioEsquema"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class UsuarioDao {
    // Enpoint Obtener perfil
    static obtenerUsuarios(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const datos = yield UsuarioEsquema_1.default.find().sort({ _id: -1 });
            res.status(200).json(datos);
        });
    }
    //Enpoint crear
    static crearUsuario(correo, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //validar la existencia del dato
            const existe = yield UsuarioEsquema_1.default.findOne(correo);
            if (existe) {
                res.status(400).json({ respuesta: "El correo ya existe" });
            }
            else {
                parametros.claveUsuario = bcryptjs_1.default.hashSync(parametros.claveUsuario, 8);
                const objUsuario = new UsuarioEsquema_1.default(parametros); // objeto perfil esquema
                objUsuario.save((miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede crear el usuario" });
                    }
                    else {
                        const datosVisibles = {
                            codUsuario: miObjeto._id,
                            correo: correo
                        };
                        const llavePrivada = String(process.env.clave_secreta);
                        const miToken = jsonwebtoken_1.default.sign(datosVisibles, llavePrivada, { expiresIn: 86400 }); //habilitar tocken
                        res
                            .status(200)
                            .json({ token: miToken });
                    }
                });
            }
        });
    }
    //Eliminar
    static eliminarUsuario(parametro, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield UsuarioEsquema_1.default.findById(parametro).exec();
            if (existe) {
                //UsuarioEsquema.findByIdAndDelete( parametro , (miError: any, miObjeto: any)
                UsuarioEsquema_1.default.findByIdAndDelete(parametro, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede eliminar el usuario" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            resultado: "Usuario eliminado:",
                            eliminado: miObjeto
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El usuario no existe" });
            }
        });
    }
    static actualizarUsuario(codigo, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //const nuevoCodigo = new Types.ObjectId (codigo)
            //const existe = await PerfilEsquema.findById(codigo);
            //const existe = await PerfilEsquema.findById({_id:codigo});
            const existe = yield UsuarioEsquema_1.default.findById(codigo).exec();
            if (existe) {
                UsuarioEsquema_1.default.findByIdAndUpdate({ _id: codigo }, { $set: parametros }, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede actualizar el usuario" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            resultado: "Usuario Actualizado:",
                            antiguo: miObjeto,
                            nuevo: parametros
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El usuario a actualizar NO existe" });
            }
        });
    }
}
exports.default = UsuarioDao;
