"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PerfilEsquema_1 = __importDefault(require("../esquema/PerfilEsquema"));
class PerfilDao {
    // Enpoint Obtener perfil
    static obtenerPerfiles(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const datos = yield PerfilEsquema_1.default.find().sort({ _id: -1 });
            res.status(200).json(datos);
        });
    }
    //Enpoint crear
    static crearPerfil(parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //validar la existencia del dato
            const existe = yield PerfilEsquema_1.default.findOne(parametros);
            if (existe) {
                res.status(400).json({ respuesta: "El perfil ya existe" });
            }
            else {
                const objPerfil = new PerfilEsquema_1.default(parametros); // objeto perfil esquema
                objPerfil.save((miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede crear el perfil" });
                    }
                    else {
                        res
                            .status(200)
                            .json({ respuesta: "Perfil creado", codigo: miObjeto._id });
                    }
                });
            }
        });
    }
    //Eliminar
    static eliminarPerfil(parametro, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const existe = yield PerfilEsquema_1.default.findById(parametro).exec();
            if (existe) {
                PerfilEsquema_1.default.findByIdAndDelete(parametro, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede eliminar el perfil" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            resultado: "Perfil eliminado:",
                            eliminado: miObjeto
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El perfil no existe" });
            }
        });
    }
    static actualizarPerfil(codigo, parametros, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //const nuevoCodigo = new Types.ObjectId (codigo)
            //const existe = await PerfilEsquema.findById(codigo);
            //const existe = await PerfilEsquema.findById({_id:codigo});
            const existe = yield PerfilEsquema_1.default.findById(codigo).exec();
            if (existe) {
                PerfilEsquema_1.default.findByIdAndUpdate({ _id: codigo }, { $set: parametros }, (miError, miObjeto) => {
                    if (miError) {
                        res.status(400).json({ respuesta: "No se puede actualizar el perfil" });
                    }
                    else {
                        res
                            .status(200)
                            .json({
                            resultado: "Perfil Actualizado:",
                            antiguo: miObjeto,
                            nuevo: parametros
                        });
                    }
                });
            }
            else {
                res.status(400).json({ respuesta: "El a actualizar No existe" });
            }
        });
    }
}
exports.default = PerfilDao;
