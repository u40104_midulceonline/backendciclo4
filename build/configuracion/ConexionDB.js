"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ConexionDB = () => {
    const urlConexion = String(process.env.db_mongo);
    (0, mongoose_1.connect)(urlConexion)
        .then(() => {
        console.log("Conectados a la base de datos", process.env.db_mongo);
    })
        .catch((miError) => {
        console.log("NO se puede establecer una conexion", miError);
    });
};
exports.default = ConexionDB;
