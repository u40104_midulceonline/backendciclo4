"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PerfilDao_1 = __importDefault(require("../dao/PerfilDao"));
class PerfilControlador extends PerfilDao_1.default {
    consulta(req, res) {
        //llamamos al dao
        PerfilControlador.obtenerPerfiles(res);
    }
    crear(req, res) {
        //llamamos al dao
        PerfilControlador.crearPerfil(req.body, res);
    }
    eliminar(req, res) {
        //llamamos al dao
        PerfilControlador.eliminarPerfil(req.params.codigo, res);
    }
    actualizar(req, res) {
        //llamamos al dao
        PerfilControlador.actualizarPerfil(req.params.codigo, req.body, res);
    }
}
const perfilControlador = new PerfilControlador(); //crear objeto tipo controlador
exports.default = perfilControlador;
