"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioEntidad = void 0;
class UsuarioEntidad {
    constructor(nom, cor, cla, fec, est, cod) {
        this.nombreUsuario = nom;
        this.correoUsuario = cor;
        this.claveUsuario = cla;
        this.fechaRegistroUsuario = fec;
        this.estadoUsuario = est;
        this.codPerfil = cod;
    }
}
exports.UsuarioEntidad = UsuarioEntidad;
exports.default = UsuarioEntidad;
